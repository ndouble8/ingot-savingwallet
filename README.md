<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p></p>
<p>SAVING WALLET</p>
## About SAVING WALLET

Saving wallet is a role based application that is secure and would help you keep track of your assets.
- Built using Laravel FrameWork (version 8)
- Uses MySQL db
- Secure (authenticated and authorized)
- Modern design


## How To Run

1. Please install a server or HomeStead on your machine to run php (HomeStead is advised since it has all the extensions and dependencies).
    already installed to vagrant virtual box (Laravel/Homestead) [setup steps](https://laravel.com/docs/8.x/homestead)
2. Download the project and add the host name (savingwallet.test as defined in the .env) and ip to your host file on your server.
3. After Running the app, you must
    1. Add a new db schema to your sql under the name 'savingwallet'.
    2. php artisan migrate to build the tables
    3. php artisan db:seed -> this will create an 
        1. admin user you can sign in with 
            1. email-> (rousan.noura@gmail.com)
            2. password-> (helloworld)
        2. will fill the roles and user_roles table
        3. will add predefined categories to the categories table
    4. go to savingwallet.test 


## Know more about the application
1. The application is made up of 5 main main pages 
    1. welcome page 
    2. sign in page 
    3. register page
    4. user wallet page
    5. admin page
    
2. the user can enter his one wallet page and ofc can register and login
3. the admin can enter his admin page and ofc login
4. the registration page is made only for normal_user (you can't register and become an admin)
5. in the wallet page the user can 
    1. view all of their transactions 
    2. can add an expense transactions 
    3. and income transactions
    4. can add customized income or expense category
    5. can view balance total incomes and expenses
6. in the admin page the user can view all users info except for password 

## Genral Notes during coding phase
1. using sql as a data base.
2. a small package for ui purposes has been added, you can find the js and css files under public-> adminlte.
3. all required js and css files for the site have been added to the shared header and footer which can be found under views->_shared.   
4. required attributes for user have been added to the predefined migration file provided by laravel (since there isn't any delete operation a soft delete column was not added).
5. the database schema consists of 5 Tables 1-Users 2-Roles 3-UserRoles 4-Transactions 5-Categories
6. in user table the balance is set to zero by default(ofc best practice is for the user to have a profile page where they can update their balance).
7. clarification: the categories table will contain both defined and predefined categories, it has a relationship with the user table zero to many,
  as the foriegn key is specified to nullable, this way both types of categories can be in the same table(easier schema); as the existane of a category doesn't depend on a user.
8. an admin is created through seeding the db, after migrating you should seed the db.(the password is hello world)
9. the design idea behind the db schema was to have all users in the same table (admins and normal users); a m2m releationship with the roles table was made.
10. accordingly authentication and autherization was implented using Manual Authentication and Gates in the Autherization proccess.
11. uploaded images are saved in the public folder and the pathes are saved in the db.
12. I apologies the css isn't exactly in place but due to shortage of time
13. a guest user can't enter admin or users pages
    b.an admin can't enter a user page or action and vise versa
    c.a user can't enter another user's page or action

## Security Vulnerabilities
there could be vulnerabilities, given the time and experience

## Questions

call or email me at anytime :)
Noura Al Rousan
