<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function viewUserData(int $id)
    {
        $this->authorize('same-user', $id);
        $users = $this->getUsersData();
        return view('view_user_data', ['users' => $users]);
    }


    private function getUsersData()
    {
        $usersIncome = User::selectRaw('users.id, users.name, users.email, users.phone_number, users.balance, users.birth_date, users.created_at,
                             SUM(transactions.amount) AS income_amount')
                             ->leftJoin('transactions', 'transactions.user_id', '=', 'users.id')
                             ->where('transactions.type', 'income')
                             ->groupby('transactions.type','users.id')
                             ->get();
        $usersExpense = User::selectRaw('users.id, users.name, users.email, users.phone_number, users.balance, users.birth_date, users.created_at,
                              SUM(transactions.amount) AS expense_amount')
                             ->leftJoin('transactions', 'transactions.user_id', '=', 'users.id')
                             ->where('transactions.type', 'expense')
                             ->groupby('transactions.type','users.id')
                             ->get();

        $usersIncome = json_decode(json_encode($usersIncome), true);
        $usersExpense = json_decode(json_encode($usersExpense), true);

        $users = array_merge_recursive($usersIncome,$usersExpense);

        //a cache would have solved this
        $usersGroupedRows=[];
        foreach ($users as $user){
            if(array_key_exists($user['id'],$usersGroupedRows))
            {
                $usersGroupedRows[$user['id']] = array_merge($user,$usersGroupedRows[$user['id']]);
            }else{
                $usersGroupedRows[$user['id']] = $user;
            }
        }
        return $usersGroupedRows;
    }
}
