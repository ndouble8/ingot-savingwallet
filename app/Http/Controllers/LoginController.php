<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->remember_token)) {
            $request->session()->regenerate();
            $user = $user = Auth::user();

            $userRoles = $user->roles;

            foreach ($userRoles as $Role)
            {
                if ($Role->name == 'admin')
                {
                    return redirect()->intended('admin/'.$user->id);
                }
            }
            return redirect()->intended('my_wallet/'. $user->id);

        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.'
        ]);
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        Session::flush();

        return redirect('/');
    }
}
