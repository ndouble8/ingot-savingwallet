<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;


class UserController extends Controller
{
    /**
     * Post Add User
     *
     * @method Post
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addUser(Request $request){

         $request->validate([
            'name' => 'required|alpha',
            'email' => 'required|email',
            'phone_number' => 'required|numeric',
            'password' => 'required|min:8|alpha_num',
            'user_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000',
        ]);

        try {
            $imageName = time().'.'.$request->user_image->extension();
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone_number = $request->phone_number;
            $user->password = Hash::make($request->password);
            $user->birth_date = $request->birth_date;
            $user->user_image = env('APP_URL'). '/images/'. $imageName;
            $request->user_image->move(public_path('images'), $imageName);
            $user->save();
            $user->roles()->attach(1);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        return Redirect::route('SignIn');
    }

    /**
     * Get User Wallet
     *
     * @method Get
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getUserWallet(int $id, Request $request){


        if($id != Auth::id()){
            abort(403);
        }

        $user = User::find($id);

        if (!$user) {
            return redirect()->route('/register');
        }

        // Initialize data sent to the view
        $data = ['balance' => 0, 'total_of_incomes' => 0, 'total_of_expenses' => 0, 'income_categories' => [], 'expense_categories' => []];

        // Get the predefined and customized categories to send in data
        //check if saved in cache
        $predefinedCategories = $this->getPredefinedCategories();
        $predefinedCategories = json_decode(json_encode($predefinedCategories), true);
        $customizedCategories = $this->getCustomizedCategories($id);
        $customizedCategories = json_decode(json_encode($customizedCategories), true);

        $incomeCategories = array_merge($predefinedCategories['income_categories'], $customizedCategories['income_categories']);
        $expenseCategories = array_merge($predefinedCategories['expense_categories'], $customizedCategories['expense_categories']);

        // Get the transactions totals to send in data
        //if in cache get them from cache else
        $transactionsTotals = $this->fetchUserTotals($id);

        // Get the User transactions to send in data
        //if in cache get them from cache else
        $transactions = $this->fetchUserTransactions($id);

        // Prepare data
        $data['balance'] = $user->balance;
        $data['user_name'] = $user->name;
        $data['total_of_incomes'] = isset($transactionsTotals['total_of_incomes']['amount'])? $transactionsTotals['total_of_incomes']: 0;
        $data['total_of_expenses'] = isset($transactionsTotals['total_of_expenses']['amount'])?$transactionsTotals['total_of_expenses']:0;
        $data['transactions'] = $transactions;
        $data['income_categories'] = $incomeCategories;
        $data['expense_categories'] = $expenseCategories;
        $data['warning'] = session()->get( 'warning' );
        return view('my_wallet', ['id'=>$id, 'data' => $data]);

    }

    public function saveUserTransactions(int $id, Request $request)
    {
        //save new balance to db and fetchUserTotals and save them to cache also fetchUserTransactions to cache
        $request->validate([
            'category' => 'required',
            'amount' => 'required|numeric'
        ]);

        $this->authorize('same-user', $id);

        try {
            $category = Category::where('name', $request->category)
                                ->where('user_id', $id)
                                ->orwhereNull('user_id')
                                ->where('name', $request->category)
                                ->first();

            $user = User::find($id);
            $transaction = new Transaction();
            if($request->income){
                $transaction->type = 'income';
                //user update balance
                $newBalance = $user['balance'] + $request->amount;
                $user->update(array('balance' => $newBalance));
            }else{
                $transaction->type = 'expense';
                //user update balance
                if($request->amount > $user['balance']){
                    return Redirect::route('UserWallet', ['id'=>$id]);
                }
                $newBalance = $user['balance'] - $request->amount;
                $user->update(array('balance' => $newBalance));
            }
            $transaction->amount = $request->amount;
            $transaction->note = $request->note;
            $transaction->category()->associate($category->id);
            $user->transactions()->save($transaction);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return Redirect::route('UserWallet', ['id'=>$id])->with( ['warning' => 'You can\'t spend more than you get!'] );
    }

    public function saveUserCategories($id, Request $request)
    {
        //save categories to cache
        $request->validate([
            'custom_category' => 'required|alpha'
        ]);

        $this->authorize('same-user', $id);

        try {
            $user = User::find($id);
            $category = new Category();
            if($request->income){
                $category->type = 'income';
            }else{
                $category->type = 'expense';
            }
            $category->name = $request->custom_category;
            $user->categories()->save($category);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return Redirect::route('UserWallet', ['id'=>$id]);
    }


    private function fetchUserTotals(int $id)
    {
        $totalOfIncomes = Transaction::selectRaw("SUM(amount) AS amount")
                                     ->where('user_id', $id)
                                     ->where('type','income')
                                     ->first();


        $totalOfExpenses = Transaction::selectRaw("SUM(amount) AS amount")
                                      ->where('user_id', $id)
                                      ->where('type','expense')
                                      ->first();

        $totals = ['total_of_incomes' => $totalOfIncomes, 'total_of_expenses' => $totalOfExpenses];
        return $totals;
    }

    private function fetchUserTransactions(int $id)
    {
        $transactions = Transaction::select('transactions.amount', 'transactions.note', 'transactions.type','categories.name')
                                     ->leftJoin('categories', 'transactions.category_id', '=', 'categories.id')
                                     ->where('transactions.user_id', $id)
                                     ->get();
        return $transactions;
    }

    private function getPredefinedCategories()
    {
        $incomeCategories = Category::select('name')
                                    ->whereNull('user_id')
                                    ->where('type','income')
                                    ->get();
        $expenseCategories = Category::select('name')
                                     ->whereNull('user_id')
                                     ->where('type','expense')
                                     ->get();
        $categories = ['income_categories' => $incomeCategories, 'expense_categories' => $expenseCategories];
        return $categories;
    }

    private function getCustomizedCategories($id)
    {
        $incomeCategories = Category::select('name')
                                    ->where('user_id', $id)
                                    ->where('type','income')
                                    ->get();
        $expenseCategories = Category::select('name')
                                    ->where('user_id', $id)
                                    ->where('type','expense')
                                    ->get();
        $categories = ['income_categories' => $incomeCategories, 'expense_categories' => $expenseCategories];
        return $categories;
    }
}
