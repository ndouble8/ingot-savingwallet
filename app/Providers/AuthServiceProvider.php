<?php

namespace App\Providers;

use App\Models\Transaction;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('restricted-to-admin', function ($user) {
            $userRoles = $user->roles;
            foreach ($userRoles as $Role){
                if ($Role->name == 'admin'){
                    return true;
                }
            }
            return false;
        });

        Gate::define('restricted-to-user', function ($user) {
            $userRoles = $user->roles;
            foreach ($userRoles as $Role){
                if ($Role->name == 'normal_user'){
                    return true;
                }
            }
            return false;
        });

        Gate::define('same-user', function ($user, $requestUser) {
            return $user->id === (int)$requestUser;
        });
    }
}
