<!DOCTYPE html>
<html>
    @include('_shared.header')
    <body class="hold-transition">
        <div class="col-sm-4 m-auto">
            <div class="content-header">
                <div class="container-fluid">
                    <div class=" col-sm-6 m-auto mb-2 mt-2 d-flex">
                        <div class="col-sm-12 mb-4 mt-4">
                            <h1 class="text-center text-dark">Welcome {{$data['user_name']}} to your WALLET</h1>
                        </div>
                        <div class="d-inline mb-4 mt-4">
                            <form method="POST" action="{{url('/logout')}}">
                                @csrf
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item d-none d-sm-inline-block">
                                        <a href="{{url('/logout')}}" alt="Sign Out" class="nav-link">
                                            <i class="fas fa-sign-out-alt">
                                                <input type="submit" class="d-none">
                                            </i>
                                        </a>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class=" d-flex m-auto">
                        <div class="card card-primary mr-5 ml-5" style="min-width: 40em" >
                            <div class="card-header">
                                <h3 class="card-title">Incomes</h3>
                            </div>
                            <form method="POST" action="{{url('/my_wallet/'. $id . '/add_transaction')}}">
                                @csrf
                                <input type="hidden" name="income" value="true">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="SelectIncomeCategory" class="col-sm-2 col-form-label">Category</label>
                                        <select class="custom-select col-sm-10" name="category" id="SelectIncomeCategory">
                                            @foreach($data['income_categories'] as $category)
                                                <option>{{$category['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->get('category'))
                                        <div>
                                            <ul>
                                                @foreach ($errors->get('category') as $error)
                                                    <li class=" badge-danger">{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="form-group row">
                                        <label for="inputAmount" class="col-sm-2 col-form-label">Amount</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="amount" class="form-control" id="inputAmount" placeholder="Amount">
                                        </div>
                                    </div>
                                    @if ($errors->get('amount'))
                                        <div>
                                            <ul>
                                                @foreach ($errors->get('amount') as $error)
                                                    <li class=" badge-danger">{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="form-group row">
                                        <label for="inputTextArea" class="col-sm-2 col-form-label">Note</label>
                                        <div class="col-sm-10">
                                            <textarea  name="note" class="form-control" id="inputTextArea"  rows="4" placeholder="Note">
                                            </textarea>
                                        </div>
                                    </div>
                                    @if ($errors->get('note'))
                                        <div>
                                            <ul>
                                                @foreach ($errors->get('note') as $error)
                                                    <li class=" badge-danger">{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card card-danger mr-5 ml-5" style="min-width: 40em">
                            <div class="card-header">
                                <h3 class="card-title">Expenses</h3>
                                <h1 class=" badge-danger">{{isset($warning)? $warning: ''}}</h1>
                            </div>
                            <div class="card-body">
                                <form method="POST" action="{{url('/my_wallet/'. $id . '/add_transaction')}}">
                                    @csrf
                                    <input type="hidden" name="expense" value="true">
                                    <div class="card-body">
                                        <div class="form-group row">
                                            <label for="SelectIncomeCategory" class="col-sm-2 col-form-label">Category</label>
                                            <select class="custom-select col-sm-10" name="category" id="SelectIncomeCategory">
                                                @foreach($data['expense_categories'] as $category)
                                                    <option>{{$category['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if ($errors->get('category'))
                                            <div>
                                                <ul>
                                                    @foreach ($errors->get('category') as $error)
                                                        <li class=" badge-danger">{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="form-group row">
                                            <label for="inputAmount" class="col-sm-2 col-form-label">Amount</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="amount" class="form-control" id="inputAmount" placeholder="Amount">
                                            </div>
                                        </div>
                                        @if ($errors->get('amount'))
                                            <div>
                                                <ul>
                                                    @foreach ($errors->get('amount') as $error)
                                                        <li class=" badge-danger">{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="form-group row">
                                            <label for="inputTextArea" class="col-sm-2 col-form-label">Note</label>
                                            <div class="col-sm-10">
                                            <textarea  name="note" class="form-control" id="inputTextArea"  rows="4" placeholder="Note">
                                            </textarea>
                                            </div>
                                        </div>
                                        @if ($errors->get('note'))
                                            <div>
                                                <ul>
                                                    @foreach ($errors->get('note') as $error)
                                                        <li class=" badge-danger">{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="footer">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class=" d-flex m-auto">
                        <div class="card card-primary mr-5 ml-5" style="min-width: 40em" >
                            <div class="card-header">
                                <h3 class="card-title">Custom Income Category</h3>
                            </div>
                            <form method="POST" action="{{url('/my_wallet/'. $id .'/add_custom_category/')}}" >
                                @csrf
                                <input type="hidden" name="income" value="true">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="inputCustomCategory" class="col-sm-2 col-form-label">Category</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="custom_category" class="form-control" id="inputCustomCategory" placeholder="Optional Custom Category">
                                        </div>
                                    </div>
                                    @if ($errors->get('custom_category'))
                                        <div>
                                            <ul>
                                                @foreach ($errors->get('custom_category') as $error)
                                                    <li class=" badge-danger">{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card card-danger mr-5 ml-5" style="min-width: 40em">
                            <div class="card-header">
                                <h3 class="card-title">Custom Expense Category</h3>
                            </div>
                            <div class="card-body">
                                <form method="POST" action="{{url('/my_wallet/'. $id .'/add_custom_category/')}}">
                                    @csrf
                                    <input type="hidden" name="expense" value="true">
                                    <div class="card-body">
                                        <div class="form-group row">
                                            <label for="inputCustomCategory" class="col-sm-2 col-form-label">Category</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="custom_category" class="form-control" id="inputCustomCategory" placeholder="Optional Custom Category">
                                            </div>
                                        </div>
                                        @if ($errors->get('custom_category'))
                                            <div>
                                                <ul>
                                                    @foreach ($errors->get('custom_category') as $error)
                                                        <li class=" badge-danger">{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="footer">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 m-auto">
                        <!-- Profile Image -->
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <h3 class="profile-username text-center">Wallet Tranasations</h3>
                                <p class="text-muted text-center">Summery</p>
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Wallet Balance</b> <a class="float-right">{{isset($data['balance'])? $data['balance']: 0}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Total Of Income</b> <a class="float-right">{{isset($data['total_of_incomes']->amount)? $data['total_of_incomes']->amount: 0}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Total Of Expenses</b> <a class="float-right">{{isset($data['total_of_expenses']->amount)? $data['total_of_expenses']->amount: 0}}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr class="text-center">
                                    <th>Transation Amount</th>
                                    <th>Transation Note</th>
                                    <th>Transaction Type</th>
                                    <th>Transaction Category</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(count($data['transactions']))
                                @foreach ($data['transactions'] as $transaction)
                                    <tr class="text-center">
                                        <td>{{$transaction['amount']}}</td>
                                        <td>{{$transaction['note']}}</td>
                                        <td>{{$transaction['type']}}</td>
                                        <td>{{$transaction['name']}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7">
                                        No records found!
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </body>
    @include('_shared.footer')
</html>





