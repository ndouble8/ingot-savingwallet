<footer class="main-footer ml-0 text-center">
	<strong> See All Your Finances In One Place ...<a href="{{env('APP_URL')}}">SAVING WALLET</a>.</strong>
</footer>
</div>
<script src="{{env('APP_URL')}}/adminlte/plugins/jquery/jquery.min.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript">
	$.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{env('APP_URL')}}/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/chart.js/Chart.min.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/sparklines/sparkline.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/jquery-knob/jquery.knob.min.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/moment/moment.min.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/daterangepicker/daterangepicker.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/summernote/summernote-bs4.min.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="{{env('APP_URL')}}/adminlte/dist/js/adminlte.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/select2/js/select2.full.min.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/moment/moment.min.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/jquery-1.2.6.min.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/jquery-ui-personalized-1.5.2.packed.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/sprinkle.js"></script>
<!-- Clean later
<script src="{{env('APP_URL')}}/adminlte/dist/js/pages/dashboard.js"></script>
<script src="{{env('APP_URL')}}/adminlte/dist/js/demo.js"></script>
-->

<script src="{{env('APP_URL')}}/adminlte/plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="{{env('APP_URL')}}/adminlte/plugins/toastr/toastr.min.js"></script>
<script type="text/javascript">
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });
</script>
