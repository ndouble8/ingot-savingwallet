<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Saving Wallet</title>
    <link rel="icon" href="{{env('APP_URL')}}/images/wallet.png" type="image/icon type">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{env('APP_URL')}}/adminlte/plugins/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="{{env('APP_URL')}}/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
	<link rel="stylesheet" href="{{env('APP_URL')}}/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<link rel="stylesheet" href="{{env('APP_URL')}}/adminlte/plugins/jqvmap/jqvmap.min.css">
	<link rel="stylesheet" href="{{env('APP_URL')}}/adminlte/dist/css/adminlte.min.css">
	<link rel="stylesheet" href="{{env('APP_URL')}}/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<link rel="stylesheet" href="{{env('APP_URL')}}/adminlte/plugins/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" href="{{env('APP_URL')}}/adminlte/plugins/summernote/summernote-bs4.css">
	<link rel="stylesheet" href="{{env('APP_URL')}}/adminlte/plugins/toastr/toastr.min.css">
	<link rel="stylesheet" href="{{env('APP_URL')}}/adminlte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
