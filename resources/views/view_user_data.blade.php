<!DOCTYPE html>
<html>
@include('_shared.header')
    <body class="hold-transition">
    <div class="col-sm-4 m-auto">
            <div class="content-header">
                <div class="container-fluid">
                    <div class=" col-sm-6 m-auto mb-2 mt-2 d-flex">
                        <div class="col-sm-12 mb-4 mt-4">
                            <h1 class="text-center text-dark">Saving Wallet.....</h1>
                        </div>
                        <div class="d-inline mb-4 mt-4">
                            <form method="POST" action="{{url('/logout')}}">
                                @csrf
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item d-none d-sm-inline-block">
                                        <a href="{{url('/logout')}}" alt="Sign Out" class="nav-link">
                                            <i class="fas fa-sign-out-alt">
                                                <input type="submit" class="d-none">
                                            </i>
                                        </a>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                        <thead>
                        <tr class="text-center">
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Birthday</th>
                            <th>Total Expenses</th>
                            <th>Total Incomes</th>
                            <th>Wallet Balance</th>
                            <th>Registration Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($users))
                            @foreach ($users as $user)
                                <tr class="text-center">
                                    <td>{{$user['name']}}</td>
                                    <td>{{$user['email']}}</td>
                                    <td>{{$user['phone_number']}}</td>
                                    <td>{{$user['birth_date']}}</td>
                                    <td>{{isset($user['expense_amount'])? $user['expense_amount']: '-'}}</td>
                                    <td>{{isset($user['income_amount'])? $user['income_amount']: '-'}}</td>
                                    <td>{{$user['balance']}}</td>
                                    <td>{{$user['created_at']}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7">
                                    No records found!
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    </body>
@include('_shared.footer')
</html>






