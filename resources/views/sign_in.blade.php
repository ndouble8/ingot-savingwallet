<!DOCTYPE html>
<html>
    @include('_shared.header')
    <body class="hold-transition">
        <div class="col-sm-4 m-auto">
            <div class="content-header">
                <div class="container-fluid">
                    <div class=" col-sm-6 m-auto mb-2 mt-2">
                        <div class="col-sm-12 mb-4 mt-4">
                            <h1 class="text-center text-dark">Saving Wallet</h1>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content" style="min-height: 35em; max-height: 35em">
                <div class="container-fluid m-auto pt-5 pb-5">
                    <div class="card card-primary  mt-5 mb-5 text-center">
                        <div class="card-header">
                            <h3 class="card-title float-none text-center">SignIn</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 m-auto">
                                    <form method="POST" action="{{url('/login')}}">
                                        @csrf
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="InputEmail1">Email address</label>
                                                <input type="email" name="email" class="form-control" id="InputEmail1" placeholder="Enter email">
                                            </div>
                                            <div class="form-group">
                                                <label for="InputPassword">Password</label>
                                                <input type="password" name="password" class="form-control" id="InputPassword" placeholder="Password">
                                            </div>
                                            @if ($errors->any())
                                                <div class="mr-5">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li class=" badge-danger">{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <div class="form-check">
                                                <input type="checkbox" name="remember_token" class="form-check-input" id="Check">
                                                <label class="form-check-label" for="Check">Remember Me Later </label>
                                            </div>
                                        </div>
                                        <div class="footer">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </body>
    @include('_shared.footer')
</html>



