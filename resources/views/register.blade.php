<!DOCTYPE html>
<html>
    @include('_shared.header')
    <body class="hold-transition">
        <div class="col-sm-6 m-auto">
            <div class="content-header">
                <div class="container-fluid">
                    <div class=" col-sm-6 m-auto mb-2 mt-2">
                        <div class="col-sm-12 mb-4 mt-4">
                            <h1 class="text-center text-dark">Saving Wallet</h1>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content">
                <div class="container-fluid m-auto pb-5">
                    <div class="card card-primary  mt-5 mb-5 text-center">
                        <div class="card-header">
                            <h3 class="card-title float-none text-center">Register</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 m-auto">
                                    <form method="POST" action="{{url('/add-user')}}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="name" class="form-control" id="inputName" placeholder="Name">
                                                </div>
                                            </div>
                                            @if ($errors->get('name'))
                                            <div>
                                                <ul>
                                                    @foreach ($errors->get('name') as $error)
                                                        <li class=" badge-danger">{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            @endif
                                            <div class="form-group row">
                                                <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                                                <div class="col-sm-10">
                                                    <input type="email" class="form-control" name="email" id="inputEmail" placeholder="Email">
                                                </div>
                                            </div>
                                            @if ($errors->get('email'))
                                                <div>
                                                    <ul>
                                                        @foreach ($errors->get('email') as $error)
                                                            <li class=" badge-danger">{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <div class="form-group row">
                                                <label for="inputPhone" class="col-sm-2 col-form-label">Phone Number</label>
                                                <div class="col-sm-10">
                                                    <input type="tel" name="phone_number" class="form-control" id="inputPhone" placeholder="Phone Number">
                                                </div>
                                            </div>
                                            @if ($errors->get('phone_number'))
                                                <div>
                                                    <ul>
                                                        @foreach ($errors->get('phone_number') as $error)
                                                            <li class=" badge-danger">{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <div class="form-group row">
                                                <label for="birth-day" class="col-sm-2 col-form-label">Birth Day</label>
                                                <div class=" col-sm-10 input-group date" id="birth-day" data-target-input="nearest">
                                                    <input type="hidden" id="birth-day" name="birth_date">
                                                    <input type="text" class="form-control datetimepicker-input" data-target="#birth-day"/>
                                                    <div class="input-group-append" data-target="#birth-day" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                                                <div class="col-sm-10">
                                                    <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Password">
                                                </div>
                                            </div>
                                            @if ($errors->get('password'))
                                                <div>
                                                    <ul>
                                                        @foreach ($errors->get('password') as $error)
                                                            <li class=" badge-danger">{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label" for="InputPicture">Personal Picture</label>
                                                <div class="input-group col-sm-10">
                                                    <div class="custom-file">
                                                        <input type="file" name="user_image" class="custom-file-input" id="InputPicture">
                                                        <label class="custom-file-label"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="footer">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </body>
    @include('_shared.footer')
</html>

<script>
    $(function () {
        //Date range picker
        $('#birth-day').datetimepicker({
            format: 'YYYY-MM-DD'
        },
        function (birth_date) {
            $('#birth-day').val(birth_date.format('YYYY-MM-DD'))
        })
    });
    $(document).ready(function () {
        bsCustomFileInput.init();
    });
</script>



