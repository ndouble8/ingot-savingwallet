<!DOCTYPE html>
<html>
    @include('_shared.header')
    <body class="hold-transition">
        <div class="col-sm-6 m-auto">
            <div class="content-header">
                <div class="container-fluid">
                    <div class=" col-sm-6 m-auto mb-2 mt-2">
                        <div class="col-sm-12 mb-4 mt-4">
                            <h1 class="text-center text-dark">Saving Wallet</h1>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content pt-5 pb-5" style="min-height: 30em">
                <div class="container-fluid m-auto pt-5 pb-5" style="min-height: inherit">
                    <div class="card card-default  mt-5 mb-5 text-center" style="min-height: 7em">
                        <div class="card-header">
                            <h3 class="card-title float-none text-center">Please Register or SignIn</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6 m-auto">
                                    <div class="card-tools">
                                        <div class="m-auto d-block">
                                            <a  href="{{url('/sign_in')}}">Sign In |</a>
                                            <a  href="{{url('/register')}}"> Register</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </body>
    @include('_shared.footer')
</html>



