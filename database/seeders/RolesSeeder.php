<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'normal_user',
                'description' => 'only permission to their profile',
            ],[
                'name' => 'admin',
                'description' => 'have access to user\'s data',
            ]
        ]);
    }
}
