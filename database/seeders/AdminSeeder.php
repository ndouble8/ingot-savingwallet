<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = Hash::make('helloworld');
        DB::table('users')->insert([
            'name' => 'noura',
            'email' => 'rousan.noura@gmail.com',
            'phone_number' => '0796288361',
            'birth_date' => '1994-8-8',
            'password' => $password,
            'user_image' => 'imagepath'
        ]);
    }
}
