<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PredefinedCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name' => 'Salary',
                'type' => 'income',
            ],[
                'name' => 'Bonuses',
                'type' => 'income',
            ],
            [
                'name' => 'Over time',
                'type' => 'income',
            ],[
                'name' => 'Food',
                'type' => 'expense',
            ],
            [
                'name' => 'Shopping',
                'type' => 'expense',
            ],[
                'name' => 'Transportation',
                'type' => 'expense',
            ]
        ]);
    }
}
