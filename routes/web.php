<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\UserController;
use \App\Http\Controllers\LoginController;
use \App\Http\Controllers\AdminController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');
Route::view('/sign_in', 'sign_in')->name('SignIn');
Route::view('/register', 'register');
Route::post('/add-user', [UserController::class,'addUser']);
Route::post('/login', [LoginController::class,'authenticate']);
Route::get('/logout', [LoginController::class,'logout']);

Route::group(['middleware' => ['can:restricted-to-user']], function () {
    Route::get('/my_wallet/{id}', [UserController::class,'getUserWallet'])->name('UserWallet');
    Route::post('/my_wallet/{id}/add_transaction', [UserController::class,'saveUserTransactions']);
    Route::post('/my_wallet/{id}/add_custom_category', [UserController::class,'saveUserCategories']);
});

Route::group(['middleware' => ['can:restricted-to-admin']], function () {
     Route::get('/admin/{id}', [AdminController::class,'viewUserData']);
});
